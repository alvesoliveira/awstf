provider "aws" {
  access_key                    = "${var.access_key}"
  secret_key                    = "${var.secret_key}"
  region                        = "${var.region}"
}

data "terraform_remote_state" "vpc" {
    backend                     = "local"

    config {
       path                     = "../vpc/terraform.tfstate"
  }
}

