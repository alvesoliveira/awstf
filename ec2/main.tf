provider "aws" {
  access_key                    = "${var.access_key}"
  secret_key                    = "${var.secret_key}"
  region                        = "${var.region}"
}

data "terraform_remote_state" "vpc" {
    backend                     = "local"

    config {
       path                     = "../vpc/terraform.tfstate"
  }
}

resource "aws_security_group" "ec2" {
   name                         = "${var.sg_name}"
   vpc_id                       = "${data.terraform_remote_state.vpc.vpc_id}"
     ingress {
     from_port			= 22
     to_port     		= 22
     protocol    		= "tcp"
     cidr_blocks 		= ["0.0.0.0/0"]
  }

     ingress {
     from_port                  = 80
     to_port                    = 80
     protocol                   = "tcp"
     cidr_blocks                = ["0.0.0.0/0"]
  }


     egress {
     from_port       		= 0
     to_port         		= 0
     protocol        		= "-1"
     cidr_blocks     		= ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami           		= "${var.ami_id}"
  instance_type			= "${var.type}"
  key_name			= "${var.key}"
  subnet_id                     = "${element(data.terraform_remote_state.vpc.public_subnets, count.index)}"
  security_groups		= [ "${aws_security_group.ec2.id}" ]
  count				= 1
}
