variable "access_key" {
        default = ""
}

variable "secret_key" {
        default = ""
}

variable "region" {
        default = "us-east-1"
}
variable "type" {
	default = "t2.micro"
}
variable "ami_id" {
	default = "ami-ba1715ac"
}
variable "sg_name" {
	default = "ec2_sg"
}

variable "key" {
	default = "terraform"
}
#variable "public_dns" {
#	default = "true"
#}
