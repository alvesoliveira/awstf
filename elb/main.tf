provider "aws" {
  access_key                    = "${var.access_key}"
  secret_key                    = "${var.secret_key}"
  region                        = "${var.region}"
}

data "terraform_remote_state" "vpc" {
    backend                     = "local"

    config {
       path                     = "../vpc/terraform.tfstate"
  }
}

data "terraform_remote_state" "ec2" {
    backend                     = "local"

    config {
       path                     = "../ec2/terraform.tfstate"
  }
}

resource "aws_security_group" "elb" {
   name                         = "${var.sg_name}"
   vpc_id                       = "${data.terraform_remote_state.vpc.vpc_id}"
   description			= "Ambiente gerenciado pela Sinestec. Favor nao alterar!"
     ingress {
     from_port   = 80
     to_port     = 80
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
  }
     egress {
     from_port       = 0
     to_port         = 0
     protocol        = "-1"
     cidr_blocks     = ["0.0.0.0/0"]
  }
}

# Create a new load balancer
resource "aws_elb" "foxmgmt" {
  name					= "${var.elb_name}"
  subnets	     			= ["${data.terraform_remote_state.vpc.public_subnets}"]

  listener {
    instance_port      			= 80
    instance_protocol  			= "http"
    lb_port            			= 80
    lb_protocol        			= "http"
  }

  health_check {
    healthy_threshold   		= 2
    unhealthy_threshold 		= 2
    timeout             		= 3
    target              		= "HTTP:80/"
    interval            		= 30
  }

#  instances                   		= ["${data.terraform_remote_state.ec2.ec2_id}"]
  cross_zone_load_balancing		= true
  idle_timeout                		= 400
  connection_draining         		= true
  connection_draining_timeout 		= 400
  security_groups    			= ["${aws_security_group.elb.id}"]
}

