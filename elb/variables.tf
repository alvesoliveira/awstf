variable "access_key" {
        default = ""
}

variable "secret_key" {
        default = ""
}

variable "region" {
        default = "us-east-1"
}
variable "elb_name" {
	default = "foxmgr"
}
variable "dns_name" {
	default = "foxmanager"
}
variable sg_name {
	default = "elb_sg"
}
