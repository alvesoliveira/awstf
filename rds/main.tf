provider "aws" {
  access_key                    = "${var.access_key}"
  secret_key                    = "${var.secret_key}"
  region                        = "${var.region}"
}

data "terraform_remote_state" "vpc" {
    backend                     = "local"

    config {
       path                     = "../vpc/terraform.tfstate"
  }
}

resource "aws_db_subnet_group" "default" {
  name                          = "${var.subnet_group_name}"
  subnet_ids                     = ["${data.terraform_remote_state.vpc.private_subnets}"]
}

resource "aws_security_group" "rds" {
   name                         = "${var.sg_name}"
   vpc_id                       = "${data.terraform_remote_state.vpc.vpc_id}"
     ingress {
     from_port   = 5432
     to_port     = 5432
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "default" {
  allocated_storage    			= "${var.size}"
  storage_type         			= "${var.type}"
  engine               			= "${var.engine}"
  engine_version       			= "${var.version}"
  instance_class       			= "${var.instance}"
  name              			= "${var.ident}"
  username             			= "${var.user}"
  password            			= "${var.pass}"
  db_subnet_group_name 			= "${aws_db_subnet_group.default.name}"
  backup_retention_period		= "${var.retention}"
  backup_window				= "${var.backup_window}"
  auto_minor_version_upgrade		= "${var.minor_upgrade}"
  vpc_security_group_ids		= ["${aws_security_group.rds.id}"]
  skip_final_snapshot			= "${var.skip_final_snapshot}"
}
