variable "access_key" {
        default = ""
}

variable "secret_key" {
        default = ""
}

variable "region" {
        default = "us-east-1"
}

variable "sg_name" {
		default = "sg_rds"
}

variable "subnet_group_name" {
		default = "rds sg"
}

variable "size" {
		default = "5"
}

variable "type" {
		default = "gp2"
}

variable "engine" {
		default = "postgres"
}

variable "version" {
		default = "9.6.2"
}

variable "instance" {
		default = "db.t2.micro"
}

variable "ident" {
		default = "foxmgr"
}

variable "user" {
		default = "postgres"
}

variable "pass" {
		default = "password"
}

variable "final_snapshot" {
		default = "false"
}

variable "retention" {
		default = "7"
}

variable "backup_window" {
		default = "00:00-03:00"
}

variable "minor_upgrade" {
		default = "false"
}
variable "skip_final_snapshot" {
		default = "true"
}
