provider "aws" {
  access_key		 	= "${var.access_key}"
  secret_key			= "${var.secret_key}"
  region			= "${var.region}"
}

resource "aws_vpc" "main" {
  cidr_block			= "${var.vpc_cidr}"
  enable_dns_hostnames 		= "${var.vpc_dns}"
}

resource "aws_internet_gateway" "gw" {
  vpc_id 			= "${aws_vpc.main.id}"
}

resource "aws_route_table" "r" {
  vpc_id			= "${aws_vpc.main.id}"
}

resource "aws_route_table" "s" {
  vpc_id                        = "${aws_vpc.main.id}"
  route {
     cidr_block			= "0.0.0.0/0"
     gateway_id			= "${aws_internet_gateway.gw.id}"
 }
}

resource "aws_subnet" "pub" {
  vpc_id                        = "${aws_vpc.main.id}"
  cidr_block                    = "${lookup(var.pubsubnet_cidr, format("zone%d", count.index))}"
  availability_zone             = "${lookup(var.azs, format("zone%d", count.index))}"
  map_public_ip_on_launch	= "${var.mapip}"
  count                         = "4"
}

resource "aws_subnet" "prv" {
  vpc_id                        = "${aws_vpc.main.id}"
  cidr_block                    = "${lookup(var.prvsubnet_cidr, format("zone%d", count.index))}"
  availability_zone             = "${lookup(var.azs, format("zone%d", count.index))}"
  count				= "4"
}

resource "aws_route_table_association" "a" {
  subnet_id			= "${element(aws_subnet.prv.*.id, count.index)}"
  route_table_id		= "${aws_route_table.r.id}"
  count				= "3"
}

resource "aws_route_table_association" "b" {
  subnet_id                     = "${element(aws_subnet.pub.*.id, count.index)}"
  route_table_id                = "${aws_route_table.s.id}"
  count				= "3"
}   
