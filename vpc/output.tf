output "private_subnets" {
  value = ["${aws_subnet.prv.*.id}"]
}

output "public_subnets" {
  value = ["${aws_subnet.pub.*.id}"]
}

output "vpc_id" {
  value = "${aws_vpc.main.id}"
}

output "vpc_azs" {
  value = ["${aws_subnet.prv.*.availability_zone}"]
}
