variable "access_key" {
	default = ""
}

variable "secret_key" {
	default = ""
}

variable "region" {
	default = "us-east-1"
}

variable "vpc_cidr" {
	default = "172.16.0.0/16"
}
variable "vpc_dns" {
	default = "true"
}

variable "mapip" {
	default = "true"
}

variable "prvsubnet_cidr" {
	default { 
	zone0 = "172.16.0.0/24"
	zone1 = "172.16.1.0/24"
	zone2 = "172.16.2.0/24"
	zone3 = "172.16.3.0/24"
 }
}

variable "pubsubnet_cidr" {
        default {
        zone0 = "172.16.4.0/24"
        zone1 = "172.16.5.0/24"
        zone2 = "172.16.6.0/24"
	zone3 = "172.16.7.0/24"
 }
}

variable "azs" {
	default {
        zone0   = "us-east-1a"
        zone1   = "us-east-1b"
        zone2   = "us-east-1c"
	zone3	= "us-east-1e"
 }
}
